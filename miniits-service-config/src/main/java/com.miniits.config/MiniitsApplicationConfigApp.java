package com.miniits.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author: wq
 * @Date: 2018/2/22
 * @Time: 16:38
 * <p>
 * Description:
 * ***
 */
@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
public class MiniitsApplicationConfigApp {

    public static void main(String[] args) {
        SpringApplication.run(MiniitsApplicationConfigApp.class, args);
    }

}
