package com.miniits.gateway.admin.controller.user;

import com.miniits.models.user.UserModel;
import com.miniits.utils.BaseControllerUtil;
import com.miniits.utils.Result;
import com.miniits.utils.ServiceUrls;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

/**
 * @author: wq
 * @Date: 2017/10/2
 * @Time: 23:06
 * <p>
 * Description:
 * ***
 */
@RefreshScope
@RestController
@RequestMapping(ServiceUrls.API_VERSION.ADMIN_SERVICE)
@Api(value = "user", description = "用户接口")
public class UserController extends BaseControllerUtil {

    @Autowired
    private UserClient userClient;

    @Value("${context}")
    private String context;

    @Value("${server.port}")
    private String port;

    @ApiOperation(value = "查询用户列表")
    @RequestMapping(value = ServiceUrls.UserUrl.USERS, method = RequestMethod.GET)
    public Result<UserModel> search(
            @ApiParam(name = "filters", value = "过滤器", defaultValue = "")
            @RequestParam(value = "filters", required = false) String filters,
            @ApiParam(name = "sorts", value = "排序", defaultValue = "")
            @RequestParam(value = "sorts", required = false) String sorts,
            @ApiParam(name = "size", value = "页数", defaultValue = "15")
            @RequestParam(value = "size", required = false) int size,
            @ApiParam(name = "page", value = "页码", defaultValue = "1")
            @RequestParam(value = "page", required = false) int page) {
        return success(null);
    }

    @ApiOperation(value = "查询用户列表")
    @RequestMapping(value = "q", method = RequestMethod.GET)
    public Result search(@RequestParam(value = "page", required = false) int page) {
        return userClient.search(page);
    }

    @ApiOperation(value = "config test")
    @GetMapping(value = "test")
    public String test() {
        return context;
    }
}